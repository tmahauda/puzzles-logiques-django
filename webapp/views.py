from django.shortcuts import render
from minizinc import Instance, Model, Solver
import numpy as np


def load_result(model_name, solver_name):
    # Load model from file
    model = Model(model_name)
    # Find the MiniZinc solver configuration
    solver = Solver.lookup(solver_name)
    # Create an Instance of the model for solver
    instance = Instance(solver, model)
    # Solve
    return instance.solve()


def fruits_init(request):
    return render(request, 'webapp/fruits.html')


def fruits_validate(request):
    result = load_result("models/fruits.mzn", "gecode")
    years = np.array(result["years"])
    fruits = np.array(result["fruits"])

    #Pour chaque case on vérifie si c'est coché
    for x in range(6):
        for y in range(3):
            xy = str(x) + str(y)
            case = request.POST.get(xy)

            if not case:
                continue

            if int(case) == 1:
                print("coché au coordonnée x=", str(x), "y=", str(y))

                # Cas des fruits
                if x > 2:
                    x = x-2
                    fruit = fruits[y]
                    if fruit == x:
                        print("vrai pour le fruit")
                    else:
                        print("faux pour le fruit")
                else:
                    year = years[y]
                    if year == x:
                        print("vrai pour l'age")
                    else:
                        print("faux pour l'age")
            else:
                print("non coché au coordonnée x=", str(x), "y=", str(y))

    return render(request, 'webapp/fruits.html')