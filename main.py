from minizinc import Instance, Model, Solver


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Load model from file
    nqueens = Model("models/fruits.mzn")
    # Find the MiniZinc solver configuration for Gecode
    gecode = Solver.lookup("gecode")
    # Create an Instance of the n-Queens model for Gecode
    instance = Instance(gecode, nqueens)
    # Assign 4 to n
    # instance["n"] = 4
    result = instance.solve()
    # Output the array
    years = result["years"]
    fruits = result["fruits"]
    print(result["years"])
    print(result["fruits"])
